{ pkgs, stdenv, extraJS ? "", ... }:
stdenv.mkDerivation rec {
  name = "classroom-countdown-web";

  buildInputs = [
    (stdenv.mkDerivation {
      name = "babel-stuff";
      dontUnpack = true;
      dontBuild = true;
      installPhase = "mkdir -p $out/bin; ln -s ${(pkgs.callPackage ./node2nix/default.nix {}).package}/lib/node_modules/classroom-countdown/node_modules/.bin/* $out/bin";
    }
    )
  ];

  src = pkgs.nix-gitignore.gitignoreSource [] ./.;

	inherit extraJS;

	configurePhase = ''
	echo "$extraJS" > app/js/prepend.js
	'';

  NODE_PATH = "${(pkgs.callPackage ./node2nix/default.nix {}).package}/lib/node_modules/classroom-countdown/node_modules";

  installPhase = ''
    mkdir $out
    cp dist/* $out
  '';
}
