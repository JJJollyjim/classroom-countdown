function findOneDelta(timeserver) {
	return fetch(timeserver).then(function(res) {
		if (!res.ok) throw new Error("Response not okay");
		return res.json();
	}).then((json) => {
		return new Date().getTime() - json;
	});
}

async function findDelta(timeserver, reps) {
	let delta = Number.MAX_VALUE;

	for (let i = 0; i < reps; i++) {
		try {
			delta = Math.min(delta, await findOneDelta(timeserver));
		} catch (e) {
			// It's fine unless all attempts fail
		}
	}

	if (delta === Number.MAX_VALUE) {
		// All attempts failed
		throw new Error("Failed to sync time");
	}

	return delta;
}
