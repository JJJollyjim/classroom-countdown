function showText(elems, text) {
	for (let el of elems) {
		el.textContent = text;
	}
}

String.prototype.ccLeftPad = function(n, chr) {
	return chr.repeat(n - this.length) + this;
};

let render;

if (document.body.dataset.displayTarget === "tv") {
	const elems = {
		now: document.querySelectorAll(".nowDisplay"),
		countdown: document.querySelectorAll(".countdownDisplay"),
		next: document.querySelectorAll(".nextDisplay"),
		message: document.querySelectorAll(".messageDisplay")
	};

	render = function(content) {
		if (content.message) {
			document.body.setAttribute("data-day-type", "message");
			showText(elems.message, content.message);
		} else {
			document.body.setAttribute("data-day-type", "timetable");

			showText(elems.now, content.now);
			showText(elems.next, content.next);

			let cd = content.countdown;

			let cdText = `${cd.minutes().toString().ccLeftPad(2, '0')}:${cd.seconds().toString().ccLeftPad(2, '0')}`;

			if (cd.asHours() >= 1)
				cdText = `${cd.hours()}:` + cdText;

			showText(elems.countdown, cdText);
		}
	};
} else if (document.body.dataset.displayTarget === "main") {
	var increment = 0;
	var drops = "";
	var backDrops = "";

	while (increment < 100) {
		//couple random numbers to use for various randomizations
		//random number between 98 and 1
		var randoHundo = (Math.floor(Math.random() * (98 - 1 + 1) + 1));
		//random number between 5 and 2
		var randoFiver = (Math.floor(Math.random() * (5 - 2 + 1) + 2));
		//increment
		increment += randoFiver;
		//add in a new raindrop with various randomizations to certain CSS properties
		drops += '<div class="drop" style="left: ' + increment + '%; bottom: ' + (randoFiver + randoFiver - 1 + 100) + '%; animation-delay: 0.' + randoHundo + 's; animation-duration: 0.5' + randoHundo + 's;"><div class="stem" style="animation-delay: 0.' + randoHundo + 's; animation-duration: 0.5' + randoHundo + 's;"></div><div class="splat" style="animation-delay: 0.' + randoHundo + 's; animation-duration: 0.5' + randoHundo + 's;"></div></div>';
		backDrops += '<div class="drop" style="right: ' + increment + '%; bottom: ' + (randoFiver + randoFiver - 1 + 100) + '%; animation-delay: 0.' + randoHundo + 's; animation-duration: 0.5' + randoHundo + 's;"><div class="stem" style="animation-delay: 0.' + randoHundo + 's; animation-duration: 0.5' + randoHundo + 's;"></div><div class="splat" style="animation-delay: 0.' + randoHundo + 's; animation-duration: 0.5' + randoHundo + 's;"></div></div>';
	}

	document.querySelectorAll('.rain.front-row')[0].innerHTML = drops;
	document.querySelectorAll('.rain.back-row')[0].innerHTML = backDrops;


	const elems = {
		left: document.querySelectorAll(".leftText"),
		right: document.querySelectorAll(".rightText"),
		timer: document.querySelectorAll(".timer")
	};

	render = function(content) {
		if (content.message) {
			document.title = `${content.message} - WGC Classroom Countdown`;

			if (content.visible) {
				showText(elems.left, content.message + (!content.fresh ? "*" : ""));
				showText(elems.right, "");
				showText(elems.timer, "");
			}

			document.body.setAttribute("data-show-rain", "false");
		} else {
			let cd = content.countdown;
			let cdText = `${cd.minutes().toString().ccLeftPad(2, '0')}:${cd.seconds().toString().ccLeftPad(2, '0')}`;

			if (cd.asHours() >= 1) {
				cdText = `${cd.hours()}:${cdText}`;
			}

			document.title = `${cdText} until ${content.next} - WGC Classroom Countdown`;

			if (content.visible) {
				showText(elems.left, `Now: ${content.now}${!content.fresh ? "*" : ""}`);
				showText(elems.right, `Next: ${content.next}`);

				elems.timer[0].setAttribute("data-digits", cdText.match(/\d/g).length);
				document.body.setAttribute("data-show-rain", content.displayRain ? "true" : "false");

				showText(elems.timer, cdText);
			}
		}
	};
}
