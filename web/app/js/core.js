const DEBUG = false; // Offsets time sync
const    minTimeSyncInterval = moment.duration({minutes: 1, seconds: 1});
const targetTimeSyncInterval = moment.duration({minutes: 20});

const oneSecond = moment.duration({seconds: 1});

const TZNAME = "CC";
let nextTickTime, nextTimeSyncTime;
let latestInfo;
let latestInfoFresh = false;
let timeZone;
(() => {
	try {
		let data = localStorage.getItem("infoData");
		if (data) handleNewInfoData(JSON.parse(data));
	} catch (e) {
		// Can fail if localStorage is disabled by user
	}
})();
// Provisional value while time sync happens
let latestDelta = 0;

function processTimetable(tt) {
	return Object.keys(tt)
		.map((str) => {
			let match = str.match(/^(\d\d):(\d\d)$/);
			return {
				hour: match[1],
				minute: match[2],
				name: tt[str]
			};
		})
		.sort((a,b) => (a.hour*60 + a.minute) - (b.hour*60 + b.minute));
}

function dayFromDayspec(spec, timetables) {
	if (spec.indexOf("!") === 0) {
		return {
			timetable: timetables.get(spec),
			message: null
		};
	} else {
		return {
			timetable: null,
			message: spec
		};
	}
}

async function updateInfo() {
	try {
		const res = await fetch('/info.json');

		if (!res.ok) throw new Error("Response not okay");

		let data = await res.json();
		try {
			localStorage.setItem("infoData", JSON.stringify(data));
		} catch (e) {
			// Can fail if localStorage is disabled by user
		}
		try {
			handleNewInfoData(data);
		} catch (e) {
			let obj = {obj: e};
			if ("name" in e) {
				obj.name = e.name;
			}
			if ("message" in e) {
				obj.message = e.message;
			}
			if ("stack" in e) {
				obj.stack = e.stack;
			}
			kwiius_reportError("infoJsonError", e);
			throw e;
		}
		latestInfoFresh = true;
	} catch (e) {
		console.warn("updateInfo failed", e);
	}
}

function handleNewInfoData(data) {
	let info = {};

	info.tz = data.tz.replace(/^[^|]*\|/, `${TZNAME}|`);
	info.timetables = new Map(Object.entries(data.timetables).map(([name, tt]) => [name, processTimetable(tt)]));
	info.days = new Map(Object.entries(data.days).map(([date, spec]) => [date, dayFromDayspec(spec, info.timetables)]));
	info.defaults = new Map(Object.entries(data.defaults).map(([dow, spec]) => [parseInt(dow), dayFromDayspec(spec, info.timetables)]));
	info.weather = {
		default: data.weather.default,
		days: new Map(Object.entries(data.weather.days))
	};

	if (!latestInfo || info.tz !== latestInfo.tz) {
		moment.tz.add(info.tz);
		timeZone = moment.tz.setDefault(TZNAME);
	}

	latestInfo = info;
}

async function updateTimeSync() {
	nextTimeSyncTime = moment().add(minTimeSyncInterval);

	try {
		latestDelta = moment.duration(
			await findDelta("/get-time", 2),
			"ms");

		if (DEBUG) {
			latestDelta.subtract(moment.duration({days: 0, hours: 0, minutes: 0, seconds: 0}));
		}

		nextTickTime = null;
	} catch (e) {
		console.warn("Time sync failed", e);
	}
}

function maybeUpdateTimeSync() {
	let now = moment();
	if (nextTimeSyncTime && nextTimeSyncTime.isSameOrBefore(now)) {
		updateTimeSync();
		return true;
	}
	return false;
}

document.addEventListener("visibilitychange", () => {
	if (!document.hidden) {
		// We just came visible, update lest the user see old info
		tick(true);
	}
});

function tick(force = false, reschedule = false) {
	if (!latestInfo) return;

	// `subtract` chosen over `add` by trial and error. Seems to work!
	let currentTime = moment().subtract(latestDelta);

	// Doesn't really make sense to use isAfter, but it fixes a bug on FFX on my Nexus 5X
	if (!nextTickTime || currentTime.isAfter(nextTickTime) || force) {
		let cnt = content(currentTime);
		cnt.visible = !document.hidden;
		render(cnt);

		nextTickTime = currentTime.startOf("second").add(oneSecond);

		if (cnt.countdown && cnt.countdown.asMinutes() < 1) {
			maybeUpdateTimeSync();
		}

		// Things won't change for at least a second, so wait most of that time
		if (reschedule)
			setTimeout(() => tick(false, true), 700);
	} else {
		// Approaching transition, so use requestAnimationFrame for accurate updates
		// Also prevents high-frequency ticking when in a background tab (the setInterval handles the requesite low-freq ticking)
		if (reschedule)
			requestAnimationFrame(() => tick(false, true));
	}
}

function todayifyTimetable(time, tt) {
	return tt.map((period) => ({
		name: period.name,
		time: time.clone().hour(period.hour).minute(period.minute).second(0).millisecond(0)
	}));
}

function content(time) {
	let today = latestInfo.days.get(time.format("YYYY-MM-DD"))
		|| latestInfo.defaults.get(time.isoWeekday());

	let weather = latestInfo.weather.days.get(time.format("YYYY-MM-DD"))
		|| latestInfo.weather.default;

	if (today.timetable) {
		let timetable = todayifyTimetable(time, today.timetable);
		let periodIdx = timetable.length - 1;

		for (let i = 0; i < timetable.length; i++) {
			if (timetable[i].time.isAfter(time)) {
				periodIdx = i-1;
				break;
			}
		}

		if (periodIdx === timetable.length - 1) {
			return {
				message: timetable[periodIdx].name,
				fresh: latestInfoFresh
			};
		} else {
			let displayRain = false;
			if (weather === "rain") {
				if (/lunch|interval/i.test(timetable[periodIdx].name)) {
					timetable[periodIdx].name = timetable[periodIdx].name + " (Inside)";
					displayRain = true;
				}

				if (/lunch|interval/i.test(timetable[periodIdx+1].name)) {
					timetable[periodIdx+1].name = timetable[periodIdx+1].name + " (Inside)";
				}
			}

			return {
				now: timetable[periodIdx].name,
				next: timetable[periodIdx + 1].name,
				fresh: latestInfoFresh,
				displayRain: displayRain,

				countdown: moment.duration(timetable[periodIdx + 1].time.diff(time))
			};
		}
	} else {
		return {
			message: today.message,
			fresh: latestInfoFresh
		};
	}
}

tick(false, true);

Promise.all([updateInfo(), updateTimeSync()]).then(() => tick(true, false));

setInterval(maybeUpdateTimeSync, targetTimeSyncInterval.asMilliseconds());
setInterval(updateInfo, 1000 * 60 * 10);

// requestAnimationFrame isn't run in background tabs, but we need to update the title every second.
setInterval(tick, 1000);
