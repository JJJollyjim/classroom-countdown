# SSL config based on "intermediate" setting at https://mozilla.github.io/server-side-tls/ssl-config-generator/
# Intermediate was chosen out of a (perceived?) need for android<5 support

lua_package_path '/opt/ccdeploy/lib/?.lua;;';

server {
	listen 443 ssl http2;
	listen [::]:443 ssl http2;

	root /var/www/classroom-countdown;

	index index.html;

	server_name classroomcountdown.co.nz;

	location / {
		# Immutable files
		location ~* \.(js|css|ico)$ {
			add_header Cache-Control "public, max-age=31536000, immutable";
		}

		location ~* ^/(index.html)?$ {
			add_header Content-Security-Policy   "default-src 'none'; script-src 'self'; style-src 'self' 'unsafe-inline'; img-src 'self'; connect-src 'self'";
			add_header X-Content-Security-Policy "default-src 'none'; script-src 'self'; style-src 'self' 'unsafe-inline'; img-src 'self'; connect-src 'self'";
			add_header X-WebKit-CSP              "default-src 'none'; script-src 'self'; style-src 'self' 'unsafe-inline'; img-src 'self'; connect-src 'self'";
			add_header Strict-Transport-Security "max-age=63072000; includeSubDomains; preload";
		}

		# First attempt to serve request as file, then
		# as directory, then fall back to displaying a 404.
		try_files $uri $uri/ =404;
	}

	location = /weather_config.html {
		return 404;
	}

	location ~* "/weather_config/([a-z0-9]{30})" {
		default_type text/html;

		access_by_lua_block {
			local aa = require("admin_auth")
			aa.verify_and_set(aa.get(), ngx.var[1]);
		}

		try_files '/weather_config.html' =404;
	}

	location = /weather_config/chemtrail {
		default_type text/html;

		access_by_lua_file '/opt/ccdeploy/lib/admin_auth_handler.lua';
		content_by_lua_file '/opt/ccdeploy/lib/chemtrails.lua';
	}

	location = /get-time {
		default_type 'application/json';
		add_header Cache-Control "no-cache, no-store, must-revalidate";

		content_by_lua_block {
			ngx.say(tostring(math.floor(ngx.now()*1000)))
		}

	}

	# Legacy subdirectory
	location ~* ^/wgc(.*) {
		return 301 $scheme://classroomcountdown.co.nz$1$is_args$query_string;
	}

	ssl_certificate /etc/letsencrypt/live/classroomcountdown.co.nz/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/classroomcountdown.co.nz/privkey.pem;
	ssl_session_timeout 1d;
	ssl_session_cache shared:SSL:50m;
	ssl_session_tickets off;

	# modern configuration.
	ssl_protocols TLSv1.2;
	ssl_ciphers 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256';
	ssl_prefer_server_ciphers on;

	# HSTS (ngx_http_headers_module is required) (max-age = 2 years)
	add_header Strict-Transport-Security "max-age=63072000; includeSubDomains; preload";

}

# http://* -> https://correct
server {
	listen 80;
	listen [::]:80;

	server_name classroomcountdown.co.nz www.classroomcountdown.co.nz wgc.classroomcountdown.co.nz www.wgc.classroomcountdown.co.nz;

	return 301 https://classroomcountdown.co.nz$request_uri;
}

# https://*incorrect -> https://correct
server {
	listen 443 ssl;
	listen [::]:443 ssl;

	server_name www.classroomcountdown.co.nz wgc.classroomcountdown.co.nz www.wgc.classroomcountdown.co.nz;

	ssl_certificate /etc/letsencrypt/live/classroomcountdown.co.nz/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/classroomcountdown.co.nz/privkey.pem;

	return 301 https://classroomcountdown.co.nz$request_uri;
}
