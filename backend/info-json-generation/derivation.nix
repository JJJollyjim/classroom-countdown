{ pkgs, ... }:
with pkgs;
let
  pythonEnv = python37.withPackages (ps: [ ps.ics ps.requests ]);
  generatorCode = lib.sourceFilesBySuffices ./. [ ".py" ];
in
writeScript "update-info-json"
  ''#!${bash}/bin/bash
    set -euf -o pipefail

    TMPJSON=/var/lib/classroom-countdown/info.json.tmp
    JSON=/var/lib/classroom-countdown/info.json

    ${pythonEnv}/bin/python ${generatorCode}/main.py > $TMPJSON

    mv $TMPJSON $JSON
  ''
