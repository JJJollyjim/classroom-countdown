#!/usr/bin/env python3
"""Generates info.json from config and calendar"""

import icshandler
import configparser
import requests
import json
import os


config = configparser.ConfigParser(interpolation=None, delimiters=('='))
config.read_file(open('/var/lib/classroom-countdown/config', 'r'))

ics = requests.get(config['main']['ics']).text

timetables = {k: dict(v) for k, v in config.items() if k[0] == "!"}

with open("/var/lib/classroom-countdown/weather") as file:
    weather = json.load(file)

print(icshandler.make_json(
    ics=ics,
    defaults=config['defaults'],
    timetables=timetables,
    tz=open(os.environ['CC_TZFILE'], 'r').read(),
    weather=(
        (config['weather']['default'] if
         config.getboolean('weather', 'enabled') is True
         else None),
        weather
    )
))
