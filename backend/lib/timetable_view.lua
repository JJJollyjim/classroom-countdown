#!/usr/bin/env luajit

ngx.req.read_body()
local postargs = ngx.req.get_post_args()

conffiler = assert(io.open("/var/lib/classroom-countdown/config", "r"))
local conf = conffiler:read("*all")
conffiler:close()

function escape_not_general_purpose_safe(s)
	if s == nil then return '' end

	local esc, i = s:gsub('&', '&amp;'):gsub('<', '&lt;'):gsub('>', '&gt;')
	return esc
end

ngx.header["content-type"] = "text/html; charset=UTF-8"

ngx.say("<form method=POST action='/timetable_config_set'><textarea name=config style='width: 100%' rows='40'>" .. escape_not_general_purpose_safe(conf) .. "</textarea><input type=submit value=Submit></form>")
