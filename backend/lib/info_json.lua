local luatz = require "luatz/init"
local cjson = require "cjson"

local function get()
	infojsonf = assert(io.open("/var/lib/classroom-countdown/info.json", "r"))
	local infojson = infojsonf:read("*all")
	infojsonf:close()
	return cjson.decode(infojson)
end

local function tz(info)
	local tzname = string.match(info.tz, "^(.-)|")
	return luatz.get_tz(tzname)
end

return {
	tz=tz,
	get=get
}
