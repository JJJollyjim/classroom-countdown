#!/usr/bin/env luajit
local luatz = require "luatz/init"
local info_json = require "info_json"
local rfc3309 = require "rfc3309"
local cjson = require "cjson"

local info = info_json.get()
local tz = info_json.tz(info)

ngx.req.read_body()
local postargs = ngx.req.get_post_args()

local date = nil
if postargs.day == "today" then
	date = rfc3309.date(luatz.timetable.new_from_timestamp(tz:localise(luatz.time())))
elseif postargs.day:match("^%d%d%d%d-%d%d-%d%d$") then
	date = postargs.day
else
	error "invalid date format"
end

local weather = nil
if postargs.weather == "rain" or postargs.weather == "sun" then
	weather = postargs.weather
else
	error "invalid weather format"
end

-- Cannot use w+ as it truncates instantly, cannot use r+ as it doesn't truncate
-- at all (hence trailing bytes are left if the new write is shorter)

local dayfile = assert(io.open("/var/cc/weather", "r"))
days = cjson.decode(dayfile:read("*all"))
dayfile:close()

days[date] = weather

dayfilew = assert(io.open("/var/cc/weather", "w"))
dayfilew:write(cjson.encode(days))
dayfilew:close()

local present_tense = nil
if weather == "rain" then
	present_tense = "inside"
elseif weather == "sun" then
	present_tense = "outside"
end

ngx.say(date.." was successfully set as "..present_tense..".")
