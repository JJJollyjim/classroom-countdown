#!/usr/bin/env luajit

ngx.req.read_body()
local postargs = ngx.req.get_post_args()

if ngx.req.get_method() ~= "POST" then
	ngx.status = ngx.HTTP_NOT_ALLOWED
	ngx.say("405")
else
	local conf = postargs.config

	conffilew = assert(io.open("/var/lib/classroom-countdown/config", "w"))
	conffilew:write(conf)
	conffilew:close()

	ngx.say("done, hopefully...")
end
