local cjson = require "cjson"
local ck = require "cookie"

local function get()
	credsf = assert(io.open("/etc/classroom-countdown-creds", "r"))
	creds = cjson.decode(credsf:read("*all"))
	credsf:close()
	return creds
end

local function verify_and_set(creds, token)
	if not creds[token] then
		ngx.say("access denied")
		ngx.exit(ngx.HTTP_FORBIDDEN)
	else
		ck:new():set({
				key = "cc_admin_token",
				value = token,
				secure = true,
				httponly = true,
				samesite = "Strict",
				path = "/"
		})
	end
end

return {
	get = get,
	verify_and_set = verify_and_set
}
