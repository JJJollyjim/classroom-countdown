{ pkgs ? import <nixpkgs> {} }:
{
  webFiles = pkgs.callPackage ./web/derivation.nix {};
  luaDir = pkgs.callPackage ./backend/derivation.nix {};
}
