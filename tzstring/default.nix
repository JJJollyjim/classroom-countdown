{ stdenvNoCC, runCommand, mkYarnPackage, glibc, tzdata }:
let builder = mkYarnPackage {
			name = "tzstring-builder";
			src = ./.;
			packageJSON = ./package.json;
			yarnLock = ./yarn.lock;
		};
in
name: from: to: stdenvNoCC.mkDerivation {
	name = "moment-tzstring";
	buildInputs = [ tzdata ];
	dontUnpack = true;
	dontConfigure = true;
	dontInstall = true;
	buildPhase = ''
		${glibc.bin}/bin/zdump -v -c "${toString from},${toString to}" "${name}" | ${builder}/bin/tzstring "${name}" > $out
'';
}
