#!/usr/bin/env node

const fs = require("fs");
const moment = require("moment-timezone");
const utils = require('./moment-timezone-utils')(moment);

process.stdout.write(utils.tz.pack(dedupe(collect(fs.readFileSync(0).toString().split("\n")))));

// From data-collect.js
function collect(lines) {
	var format = "MMM D HH:mm:ss YYYY";

	var name = process.argv[2],
			abbrs   = [],
			untils  = [],
			offsets = [],
			countries = [];

	lines.forEach(function (line) {
		var parts  = line.split(/\s+/),
				utc    = moment.utc(parts.slice(2, 6).join(' '), format),
				local  = moment.utc(parts.slice(9, 13).join(' '), format);

		if (parts.length < 13) { return; }

		offsets.push(+utc.diff(local, 'minutes', true).toFixed(4));
		untils.push(+utc);
		abbrs.push(parts[13]);
	});

	if (offsets.length === 0 && lines.length === 3 && lines[2].length === 0) {
		// use alternate zdump format
		var utcParts   = lines[0].split(/\s+/),
				localParts = lines[1].split(/\s+/);

		var utc   = moment.utc(utcParts.slice(2, 6).join(' '), format),
				local = moment.utc(localParts.slice(2, 6).join(' '), format);

		offsets.push(+utc.diff(local, 'minutes', true).toFixed(4));
		untils.push(null);
		abbrs.push(localParts[6]);
	}

	return {
		name       : name,
		abbrs      : abbrs,
		untils     : untils,
		offsets    : offsets,
		population : 0,
		countries  : [],
	};
}

// From data-dedupe.js
function dedupe(zone) {
	var abbrs   = [],
			untils  = [],
			offsets = [],
			length  = zone.abbrs.length,
			i;

	for (i = length - 1; i >= 0; i--) {
		if (abbrs[0]   === zone.abbrs[i] &&
				offsets[0] === zone.offsets[i]) { continue;}

		untils.unshift(i === length - 1 ? Infinity : zone.untils[i + 1]);
		abbrs.unshift(zone.abbrs[i]);
		offsets.unshift(zone.offsets[i]);
	}

	return {
		name       : zone.name,
		abbrs      : abbrs,
		untils     : untils,
		offsets    : offsets,
		population : zone.population,
		countries  : zone.countries
	};
}
