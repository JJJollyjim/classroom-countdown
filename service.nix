{ config, lib, pkgs, ... }:
let
  cfg = config.services.classroomCountdown;
  fullExtraDomains = map (sub: "${sub}.${cfg.baseDomain}") cfg.subdomains;
  webFiles = extraJS: pkgs.callPackage ./web/derivation.nix { inherit extraJS; };
  luaFiles = pkgs.callPackage ./backend/derivation.nix {};
  infoJsonScript = pkgs.callPackage ./backend/info-json-generation/derivation.nix {};
in
  with lib;
  {
    options.services.classroomCountdown = {
      enable = mkEnableOption "loki";

      baseDomain = mkOption {
        type = types.str;
        description = ''
          Canonical hostname at which to run the service
        '';
      };

      subdomains = mkOption {
        type = types.listOf types.str;
        default = [ "www" ];
        description = "List of subdomains to redirect to the canonical hostname";
      };

      adminKeys = mkOption {
        type = types.listOf types.str;
        default = [];
        description = "Tokens to access admin functionality";
      };

			timezone = mkOption {
				type = types.str;
				description = ''
				  Olson timezone name
				'';
			};

			timezoneFrom = mkOption {
				type = types.int;
				description = ''
				  Year from which to generate timezone data
				'';
				default = 2010;
			};

			timezoneTo = mkOption {
				type = types.int;
				description = ''
				  Year to which to generate timezone data
				'';
				default = 2100;
			};

      extraJS = mkOption {
        type = types.lines;
        default = "";
        description = "Inserted in the <head> of webpages";
      };
    };

    config = mkIf cfg.enable {
      systemd.tmpfiles.rules = [
        "d /var/lib/classroom-countdown 775 nginx nginx -"
        "f /var/lib/classroom-countdown/weather 664 nginx nginx - {}"
        "f /var/lib/classroom-countdown/config 664 nginx nginx -"
      ];

      systemd.services.classroom-countdown-info-json = {
        description = "Regenerate Classroom Countdown data";
				environment = {
					"CC_TZFILE" = (pkgs.callPackage ./tzstring {}) cfg.timezone cfg.timezoneFrom cfg.timezoneTo;
				};
        serviceConfig = {
          Type = "oneshot";
          ExecStart = infoJsonScript;
          # TODO sandbox better
          User = "nginx";
          Group = "nginx";
        };
      };

      systemd.timers.classroom-countdown-info-json = {
        wantedBy = [ "timers.target" ];
        partOf = [ "classroom-countdown-info-json.service" ];
        timerConfig.OnCalendar = "minutely";
      };

      environment.etc.classroom-countdown-creds = {
        text = generators.toJSON {} (genAttrs cfg.adminKeys (_: {}));
      };

      services.nginx = {
        enable = true;

        commonHttpConfig = ''
          lua_package_path '${luaFiles}/?.lua;;';
        '';

        virtualHosts."${cfg.baseDomain}" = {
          serverName = cfg.baseDomain;

          enableACME = true;
          forceSSL = true;

          extraConfig = ''
            root ${webFiles cfg.extraJS};

            location / {
              # Immutable files
              location ~* \.(js|css|ico)$ {
                add_header Cache-Control "public, max-age=31536000, immutable";
              }

              location ~* ^/(index.html)?$ {
                add_header Content-Security-Policy   "default-src 'none'; script-src 'self'; style-src 'self' 'unsafe-inline'; img-src 'self'; connect-src 'self' https://jserr.kwiius.com";
                add_header X-Content-Security-Policy "default-src 'none'; script-src 'self'; style-src 'self' 'unsafe-inline'; img-src 'self'; connect-src 'self' https://jserr.kwiius.com";
                add_header X-WebKit-CSP              "default-src 'none'; script-src 'self'; style-src 'self' 'unsafe-inline'; img-src 'self'; connect-src 'self' https://jserr.kwiius.com";
              }

              # First attempt to serve request as file, then
              # as directory, then fall back to displaying a 404.
              try_files $uri $uri/ =404;
            }

            location = /info.json {
              alias /var/lib/classroom-countdown/info.json;
            }


            location = /weather_config.html {
              return 404;
            }

            location ~* "/weather_config/([a-z0-9]{30})" {
              default_type text/html;

              access_by_lua_block {
                local aa = require("admin_auth")
                aa.verify_and_set(aa.get(), ngx.var[1]);
              }

              try_files '/weather_config.html' =404;
            }

            location = /weather_config/chemtrail {
              default_type text/html;

              access_by_lua_file '${luaFiles}/admin_auth_handler.lua';
              content_by_lua_file '${luaFiles}/chemtrails.lua';
            }

            location ~* "/timetable_config/([a-z0-9]{30})" {
              default_type text/html;

              access_by_lua_block {
                local aa = require("admin_auth")
                aa.verify_and_set(aa.get(), ngx.var[1]);
              }

              content_by_lua_file '${luaFiles}/timetable_view.lua';
            }

            location = /timetable_config_set {
              default_type text/html;

              client_max_body_size 256k;
              client_body_buffer_size 256k;

              access_by_lua_file '${luaFiles}/admin_auth_handler.lua';
              content_by_lua_file '${luaFiles}/timetable_upload.lua';
            }

            location = /get-time {
              default_type 'application/json';
              add_header Cache-Control "no-cache, no-store, must-revalidate";

              content_by_lua_block {
                ngx.say(tostring(math.floor(ngx.now()*1000)))
              }
            }

            # Legacy subdirectory
            location ~* ^/wgc([^\s]*) {
              return 301 $scheme://${cfg.baseDomain}$1$is_args$query_string;
            }
          '';
        };

        virtualHosts."${head fullExtraDomains}" = {
          serverAliases = tail fullExtraDomains;

          enableACME = true;
          forceSSL = true;

          globalRedirect = cfg.baseDomain;

          extraConfig = ''
            add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;
          '';
        };
      };
    };
  }
